package br.com.odonto.rest;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.odonto.dao.JPAAbstract;
import br.com.odonto.entity.Treatment;

@Path("/tratamento")
public class TreatmentController extends JPAAbstract<Treatment, Serializable>{
	
	@Path("/cadastroTratamento")
	@POST
	public String cadastro(String json) throws SQLException, JSONException {
		JSONObject result = new JSONObject();
		try {
			Treatment tratamento = new ObjectMapper().readValue(json, Treatment.class);
			
			if(tratamento.getId() == null){
			    save(tratamento);
			} else {
                update(tratamento);
            }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.put("msg", "Tratamento cadastrado").toString();
	}
}
