package br.com.odonto.rest;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.odonto.dao.JPAAbstract;
import br.com.odonto.entity.Appointment;
import br.com.odonto.entity.Patient;
import br.com.odonto.util.JPAUtil;

@Path("/patient")
public class PatientController extends JPAAbstract<Patient, Serializable> {

	@POST
	@Path("/savePatient")
	public String savePatient(String json) {
		JSONObject result = new JSONObject();
		ObjectMapper mapper = new ObjectMapper();
		try{
			Patient patient = mapper.readValue(json, Patient.class);
			if (patient.getId() == null) {
				save(patient);
			} else {
				update(patient);
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return result.put("msg", "Paciente salvo com sucesso").toString();
	}

	@Path("/loadPatients")
	@GET
	public Collection<Patient> loadPatients() {
		return load();
		
	}
	
	@Path("/removePatient/{id}")
	@DELETE
	public String removePatient(@PathParam("id") Long id) {
		JSONObject result = new JSONObject();
		
		String hql = " from " + Appointment.class.getSimpleName() + " c where paciente_id = :id";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("id", id);
		
		//Se tiver algum resultado na lista, é porque o paciente está vinculado a uma consulta e não poderá ser removido.
		if(query.getResultList().size() == 0){
			remove(id);
			result.put("msg", "Paciente removido com sucesso");
		} else {
			result.put("msg", "Esse paciente está vinculado a uma consulta, não é possível remover!");
		}
		return result.toString();
	}
}
