﻿package br.com.odonto.rest;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collection;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.json.JSONException;
import org.json.JSONObject;
import br.com.odonto.dao.JPAAbstract;
import br.com.odonto.dao.UserDAO;
import br.com.odonto.entity.User;

import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/user")
public class UserController extends JPAAbstract<User, Serializable>{
	
	 UserDAO dao = new UserDAO();
	 JSONObject result = new JSONObject();
	
	@Path("/saveUser")
	@POST
	public String cadastroUsuario(String json) throws SQLException, JSONException {
		ObjectMapper mapper = new ObjectMapper();
		User oldUser = null;
		Boolean status = true;
		
		try{
		    User usuario = mapper.readValue(json, User.class);
		    if(usuario.getId() != null){
			oldUser = dao.getOldMail(usuario.getId());
		    }
		
		    /* Verifica se a senha possui menos de 4 caracteres */
	        if(usuario.getSenha().length() < 4){
	        	status = false;
	        	result.put("msg", "A senha deve conter no minimo 4 caracteres");
	        }
	        
	        /* Verifica se o e-mail está no padrão */
	        if(usuario.getEmail().indexOf("@") == -1 || usuario.getEmail().indexOf(".") == -1){
	        	status = false;
	        	result.put("msg", "Formato de e-mail inválido");
	        }
	        
	        boolean isInvalid = dao.verifyEmail(usuario.getEmail());
	        
	        /* Verifica se já existe algum usuário com o mesmo e-mail */
	        if(oldUser != null){
	        	if(!oldUser.getEmail().equals(usuario.getEmail()) && isInvalid){
	      	        status = false;
	      	        result.put("msg", "E-mail já foi cadastrado no sistema");
	        	}
	        } else if(isInvalid){
	        	status = false;
      	        result.put("msg", "E-mail já foi cadastrado no sistema");
	        }
	        
	        /* Se os dados estiverem corretos, realiza o cadastro */
	        if(status){
	        	try {
	        		usuario.setSenha(dao.criptografia(usuario.getSenha()));
					if(usuario.getId() == null){
						save(usuario);
					} else {
						update(usuario);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
	        	result.put("status", status);
				result.put("msg", "Usuario salvo com sucesso");
	        }
		} catch(Exception e){
			e.printStackTrace();
		}
        return result.toString();
	}
	
	@Path("/loadUsers")
	@GET
	public Collection<User> loadUsers(){
		return load();
	}
	
	@Path("/removeUser/{id}")
	@DELETE
	public String removeUser(@PathParam("id") Long id) {
		remove(id);
		return result.put("msg", "Usuário excluído com sucesso").toString();
	}
}
