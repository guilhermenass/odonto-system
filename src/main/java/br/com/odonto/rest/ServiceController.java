package br.com.odonto.rest;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Collection;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.odonto.dao.JPAAbstract;
import br.com.odonto.entity.Service;

@Path("/service")
public class ServiceController extends JPAAbstract<Service, Serializable> {
	
	@Path("/saveService")
	@POST
	public String saveService(String json) throws SQLException, JSONException {
		JSONObject result = new JSONObject();
		try {
			Service service = new ObjectMapper().readValue(json, Service.class);
			if(service.getServicoId() == null){
				save(service);
			} else {
				update(service);
			}
			result.put("msg", "Serviço salvo com sucesso");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	@Path("/loadServices")
	@GET
	public Collection<Service> loadServices() {
		return load();
	}
	
	@Path("/removeService/{id}")
	@DELETE
	public String removeService(@PathParam("id") Long id) {
		JSONObject result = new JSONObject();
		remove(id);
		return result.put("msg", "Serviço excluído com sucesso").toString();
	}
}
