package br.com.odonto.rest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.odonto.dao.AppointmentDAO;
import br.com.odonto.dao.JPAAbstract;
import br.com.odonto.entity.Appointment;
import br.com.odonto.entity.Patient;
import br.com.odonto.entity.Status;

@Path("/appointment")
public class AppointmentController extends JPAAbstract<Appointment, Serializable> {

	@SuppressWarnings("deprecation")
	@POST
	@Path("/saveAppointment")
	public String saveAppointment(String data) {
		Boolean status = true;
		JSONObject result = new JSONObject();
		ObjectMapper mapper = new ObjectMapper();
		try{
			Appointment consulta = mapper.readValue(data, Appointment.class);
			Timestamp horario = new Timestamp(0);
			horario.setHours(consulta.getHorario().getHours());
			horario.setMinutes(consulta.getHorario().getMinutes());

			status = new AppointmentDAO().validaConsulta(consulta.getData(), horario);

			if(status){
				if(consulta.getId() == null){
					consulta.setStatus(new Status(1L));
					save(consulta);
				} else {
					update(consulta);
				}
				result.put("msg", "Consulta salva com sucesso!");
			} else {
				result.put("msg", "Horário não disponível para consulta!");
			}

		} catch (Exception e){
			e.printStackTrace();
		}
		return result.toString();

	}
	@Path("/loadAppointments")
	@GET
	public <T> Collection<Appointment> loadAppointments() {
		return load();
	}
	
	@Path("/loadByFilter")
	@GET
	public Collection<Appointment> loadByFilter(@QueryParam("inicio") String inicio, @QueryParam("fim") String fim, @QueryParam("status") Long statusId) throws ParseException {
		
		/* Formatação de data */
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dataInicio = null;
		Date dataFim = null;
		if(inicio != null && !inicio.isEmpty() && !inicio.contains("null"))
			dataInicio = sdf.parse(inicio);
		
		if(fim != null && !fim.isEmpty() && !fim.contains("null"))
			dataFim = sdf.parse(fim);
		
		
		String hql = getQuery(inicio, fim, statusId);
		Calendar calendarInicio = Calendar.getInstance();
		Calendar calendarFim = Calendar.getInstance();
		
		if(dataInicio != null)
			calendarInicio.setTime(dataInicio);
		
		if(dataFim != null)
			calendarFim.setTime(dataFim);
		
		Query query = getEntityManager().createQuery(hql);
		if(statusId != 0)
			query.setParameter("id", statusId);
		
		if(dataInicio != null)
			query.setParameter("inicio" , calendarInicio);
		
		if(dataFim != null)
			query.setParameter("fim" , calendarFim);
		
		return query.getResultList();
	}

	
	private String getQuery(String inicio, String fim, Long statusId) {
		String hql = "from " + Appointment.class.getSimpleName() + " c where 1 = 1";
		
		if(inicio != null && !inicio.isEmpty() && !inicio.contains("null") && fim != null && !fim.isEmpty() && !fim.contains("null"))
			hql += " and data BETWEEN :inicio AND :fim";
		
		if(statusId != 0)
			hql += " and c.status.id = :id";
		
		return hql;
	}


	@Path("/loadAppointmentsDay")
	@GET
	public Collection<Appointment> loadAppointmentsDay() {
		String hql = " from " + Appointment.class.getSimpleName() + " where data = :today";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("today", GregorianCalendar.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault())));
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Path("/loadAllStatus")
	@GET
	public Collection<Status> loadAllStatus(){
		String hql = " from " + Status.class.getSimpleName();
		Query query = getEntityManager().createQuery(hql);
		return query.getResultList();
	}
	
	@Path("/loadPatientsById")
	@GET
	public Collection<Patient> loadPatientsById(){
		String hql = " select new " + Patient.class.getName() + " (p.id, p.nome) " + " from " + Patient.class.getSimpleName() + " p";
		Query query = getEntityManager().createQuery(hql);
		return query.getResultList();
	}
	
	@Path("/removeAppointment/{id}")
	@DELETE
	public String removeAppointment(@PathParam("id") Long id) {
		JSONObject result = new JSONObject();
		remove(id);
		return result.put("msg", "Consulta excluída com sucesso").toString();
	}
}
