package br.com.odonto.dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.odonto.entity.User;
import br.com.odonto.util.JPAUtil;

public class UserDAO extends JPAUtil{
	
	public boolean verifyEmail(String email) {
		String hql = " from " + User.class.getSimpleName() + " where email = :email";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("email", email);
		Boolean existe = false;
		if(query.getResultList().size() > 0){
			existe = true;
		}
		/* Se existir um usuario com o mesmo e-mail, nao será possivel efetuar o cadastro */
		return existe;
	}
	
	public User doLogin(String email, String senha) {
		String hql = " FROM " + User.class.getSimpleName() + " WHERE email = :email and senha = :senha";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("email", email);
		query.setParameter("senha", senha);
		try{
			User usuario = (User) query.getSingleResult();
			return usuario;
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	/* Método que realiza a criptografia da senha */
	public String criptografia(String senha){
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        md5.update(senha.getBytes(),0,senha.length());
        return new BigInteger(1, md5.digest()).toString(16);
	}

	public User getOldMail(Long id) {
		String hql = " from " + User.class.getSimpleName() + " where id = :id";
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("id", id);
		User usuario = (User) query.getSingleResult();
		return usuario;
	}

}
