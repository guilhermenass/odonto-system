package br.com.odonto.dao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.odonto.entity.Appointment;
import br.com.odonto.util.JPAUtil;

public class AppointmentDAO {
	
	EntityManager manager = new JPAUtil().getEntityManager();
	
	public boolean validaConsulta(Calendar data, Timestamp horario) {
		String hql = " from " + Appointment.class.getSimpleName() + " c where c.data = :data and c.horario = :horario";
		Query query = manager.createQuery(hql);
		query.setParameter("data" , data);
		query.setParameter("horario" , horario);
		List<Appointment> consultas = query.getResultList();
		
		if(consultas.size() > 0){
			return false;
		} else {
			return true;
		}
	}

}
