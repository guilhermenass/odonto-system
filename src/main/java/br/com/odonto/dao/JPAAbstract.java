package br.com.odonto.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import br.com.odonto.util.JPAUtil;

public class JPAAbstract<T, ID extends Serializable> extends JPAUtil {

	private Class<T> entity;

	@SuppressWarnings("unchecked")
	public JPAAbstract() {
		ParameterizedType superclass = (ParameterizedType) getClass().getGenericSuperclass();
		entity = (Class<T>) superclass.getActualTypeArguments()[0];
	}

	public void save(T o) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.persist(o);
		em.getTransaction().commit();
		em.close();
	}

	public T loadById(ID id) {
		EntityManager em = getEntityManager();
		return em.find(entity, id);
	}

	public Collection<T> load() {
		EntityManager em = getEntityManager();
		TypedQuery<T> query = em.createQuery(
				"SELECT t FROM " + entity.getSimpleName() + " t", entity);
		Collection<T> list = query.getResultList();
		em.close();
		return list;
	}

	public T update(T o) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.merge(o);
		em.getTransaction().commit();
		em.close();

		return o;
	}

	public void remove(ID id) {
		EntityManager em = getEntityManager();
		T o = em.find(entity, id);
		em.getTransaction().begin();
		em.remove(o);
		em.getTransaction().commit();

	}

}
