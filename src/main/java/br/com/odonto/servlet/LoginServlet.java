package br.com.odonto.servlet;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import br.com.odonto.dao.UserDAO;
import br.com.odonto.entity.User;

public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public LoginServlet() {
		super();
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException{
		try {
			process(req, resp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String process(HttpServletRequest request, HttpServletResponse response) throws SQLException{
		
		String context = request.getServletContext().getContextPath();
		UserDAO dao = new UserDAO();
		User usuario = new User();
		usuario = dao.doLogin(request.getParameter("email"), dao.criptografia(request.getParameter("senha")));
		JSONObject msg = new JSONObject();
		try {
			HttpSession sessao = request.getSession();
			if (usuario != null) {				
				sessao.setAttribute("usuario", usuario);
				response.sendRedirect(context + "/main.html");
			} else {
				msg.put("msg", "Erro ao logar");
				sessao.setAttribute("msg", "Usuario ou senha invalido!");
				response.sendRedirect(context + "/index.html?login=invalid");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return msg.toString();
	}
}
