﻿package br.com.odonto.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.odonto.entity.User;

public class ConnectionFilter implements Filter{
	
public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		/*
		 * O metodo getContexPath é responsavel por retornar o contexto da URL que realizou a requisiçao.
		 */
		String context = request.getServletContext().getContextPath();
		
		try{
			/*
			 * O metodo getSession e responsavel por pegar a sessao ativa.
			 * Aqui foi necessario fazer um casting pois o objeto request e do tipo SevletRequest e nao 
			 * HttpServletRequest como no servlet onde voce utiliza o metodo em questao sem o uso do casting.
			 */
			HttpSession session = ((HttpServletRequest)request).getSession();
			User usuario = null;
			if(session != null){
				usuario = (User) session.getAttribute("usuario");
				
			}
			if(usuario == null){
				/*
				 * Aqui esta sendo setado um atributo na sessao para que depois possamos
				 * exibir uma mensagem ao usuario
				 */
				/*
				 * Utilizamos o metodo sendRedirect que altera a URL do navegador para posicionar
				 * o usuario na tela de login, que neste caso é a pagina index.html
				 * Note que nao precisamos utilizar o recurso "../../" para informar
				 * o caminho da pagina index.html, a variavel do contexto ja posiciona no inicio da URL.
				 */
				HttpServletResponse resp =((HttpServletResponse)response);
				
				resp.sendRedirect(context + "/");
			}else{
				/*
				 * Caso exista um usuario valido (diferente de nulo) envia a requisicao para 
				 * a pagina que se deseja acessar, ou seja, permite o acesso, deixa passar.
				 */
				chain.doFilter(request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
	}

}
