var app = angular.module("odontoSystem", ['ngRoute', 'ngResource', 'angularUtils.directives.dirPagination']);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.service('OdontoService', function($http, $route, $filter) {
	
    this.save = function (data, url){	
    	$http.post(url, data).then(function successCallback(response) {
			$(".modal-backdrop").remove();
			bootbox.alert(response.data.msg);
			$route.reload();
    	},
    	function errorCallback(response) {
		     console.log(response);
		});
    }

    this.loadByFilter = function(begin, end, status){
    	return $http.get("rest/appointment/loadByFilter?begin=" + begin + "&end=" + end + "&status=" + status);
    }
    
    this.load = function (url){
		return $http.get(url);
    }
    
    this.openModal = function(path){
    	$(path).modal("show");
    }
    
    this["delete"] = function(url){
	    $http["delete"](url).then(function successCallback(response) {
	    	bootbox.alert(response.data.msg);
	    	$route.reload();
	    }, 
	    function errorCallback(response) {
		     console.log(response);
		});
    }
    
});

/* Configuração de rotas */
app.config(function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'home.html',
	})
	
	$routeProvider.when('/appointments', {
		templateUrl : 'views/appointment/appointment.html',
		controller : 'AppointmentController'
	})
	
	$routeProvider.when('/patients', {
		templateUrl : 'views/patient/patient.html',
		controller : 'PatientController'
	})
	
	$routeProvider.when('/users', {
		templateUrl : 'views/user/user.html',
		controller : 'UserController'
	})
	
	$routeProvider.when('/services', {
		templateUrl : 'views/service/service.html',
		controller : 'ServiceController'
	})
});