app.controller('AppointmentController', function($scope, $http, OdontoService, $filter) {
	$scope.appointments = [];
	
	/* Exclusão de appointments */
	$scope.removeAppointment = function(id){
		OdontoService["delete"]("rest/appointment/removeAppointment/" + id);
	};
	
	/* Load de appointments na grid */
	$scope.loadAppointments = function(){
		OdontoService.load('rest/appointment/loadAppointments').then(function(data) { 
			$scope.appointments = data.data;
			$scope.isLoading = false;
		},function (error){
			console.log(error);
		});
	}
	
	// Carrega os status das appointments
	$scope.loadAllStatus = function(){
		OdontoService.load('rest/appointment/loadAllStatus').then(function(response) {
			$scope.allStatus = response.data;
		},
		
	    function errorCallback(response) {
		     console.log(response);
		})
	}
	
	$scope.doFilter = function(begin, end, status){
		begin = begin === null || begin === undefined ? "" : $filter("date")(begin, "yyyy-MM-dd");
		end = end === null || end === undefined ? "" : $filter("date")(fim, "yyyy-MM-dd");
    	status = status === null || status === undefined ? 0 : status; 
	    OdontoService.loadByFilter(begin, end, status).then(function(data) { 
			 $scope.appointment = data.data;
		}, function (error){	
			console.log(error);
		});
	}
	
	$scope.clearFilterFields = function(){
		$scope.begin = "";
		$scope.end = "";
		$scope.appointment.statusFilter = 0;
	}
	
	$scope.loadAppointments();
	$scope.loadAllStatus();
});
