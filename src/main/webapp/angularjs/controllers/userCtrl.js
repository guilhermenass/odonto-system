app.controller('UserController', function($scope, $http, OdontoService) {
	$scope.users = [];
	
	$scope.isLoading = true;
	
	/* Exclusão de usuários */
	$scope.removeUser = function(id){
		OdontoService["delete"]("rest/user/removeUser/" + id);
	};
	
	/* Load de usuários na grid */
	$scope.loadUsers = function(){
		OdontoService.load('rest/user/loadUsers').then(function (data) { 
			$scope.users = data.data;
			$scope.isLoading = false;
		},function (error){
			console.log(error);
		});
	}
	
	$scope.loadUsers();
});