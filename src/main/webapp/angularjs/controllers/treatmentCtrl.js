app.controller('TreatmentController', function($scope, $http, OdontoService) {
	$scope.treatments = [];
	
	/* Exclusão de consultas */
	$scope.removeTreatment = function(id){
		OdontoService["delete"]("rest/treatment/removeTreatment/" + id);
	};
	
	/* Load de consultas na grid */
	$scope.loadTreatments = function(){
		OdontoService.load('rest/treatment/loadTreatments').then(function (data) { 
			$scope.treatments = data.data;
		},function (error){
			console.log(error);
		});
	}
	
	$scope.loadTreatments();
});