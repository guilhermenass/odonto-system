app.controller('ServiceController', function($scope, $http, OdontoService) {
	$scope.services = [];
	
	$scope.isLoading = true;	
	
	/* Exclusão de serviços */
	$scope.removeService = function(id){
		OdontoService["delete"]("rest/service/removeService/" + id);
	};	
	
	/* Load de serviços na grid */
	$scope.loadServices = function(){
		OdontoService.load('rest/service/loadServices').then(function (data) { 
			$scope.services = data.data;
			$scope.isLoading = false;
		},function (error){
			console.log(error);
		});
	}
	
	$scope.loadServices();
});