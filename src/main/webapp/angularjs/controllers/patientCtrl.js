app.controller('PatientController', function($scope, $http, OdontoService) {
	
	$scope.patients = [];
	
	/* Função que carrega na grid, TODOS os pacientes cadastrados */
	$scope.loadPatients = function(){
		OdontoService.load('rest/patient/loadPatients').then(function (data) { 
			$scope.patients = data.data;
			
			/* For para formatar a data no formato certo */
			for(var x = 0; x < $scope.patients.length; x++){
				$scope.patients[x].dataNascimento = new Date($scope.patients[x].dataNascimento);
			}
			
		},function (error){
			console.log(error);
		});
	}

	$scope.removePatient = function(id){
		OdontoService["delete"]("rest/patient/removePatient/" + id);
	};
	
	$scope.loadPatients();
});