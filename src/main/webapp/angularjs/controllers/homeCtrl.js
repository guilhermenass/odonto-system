app.controller('HomeController', function($scope, $http, OdontoService) {
	$scope.appointments = [];
	
	/* Load de consultas na grid */
	$scope.loadAppointmentsDay = function(){
		OdontoService.load('rest/appointment/loadAppointmentsDay').then(function(data) { 
			$scope.$parent.appointments = data.data;
		},function (error){
			console.log(error);
		});
	}
	$scope.loadAppointmentsDay();
});