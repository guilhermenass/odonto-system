app.directive("modalPatient", function(){
	return {
		templateUrl: "angularjs/directives/modal-patient.html",
		restrict: "AE",
		scope: {
			patient: '=data' 
		},
		controller: "ModalPatientController"
	};
});


angular.module('odontoSystem').controller("ModalPatientController", function ($scope, $http, OdontoService) {
	
	// Função responsável por abertura da modal para edição ou cadastro
	$scope.$parent.openModalPatient = function(patient, type){
		if(type === 'edit'){
			$scope.title = 'Edição de Pacientes';
			$scope.patient = angular.copy(patient);
		} else {
			$scope.title = 'Cadastro de Pacientes';
		}
		$("#modal-cadastro-paciente").modal("show");
	}
	
	// Cadastro de pacientes 
	$scope.savePatient = function(){
		OdontoService.save($scope.patient, "rest/paciente/cadastroPaciente");
	}
	
	// Removendo o objeto para limpar os campos da modal
	 $scope.$on("ClearFields", function(){
	  delete $scope.patient;
	 });
	
	 // Função que limpa os campos das modais de despesa e receita 
	 $scope.clearFields = function(){
	  $scope.$emit("ClearFields");
	 }
})