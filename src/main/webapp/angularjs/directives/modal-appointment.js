app.directive("modalAppointment", function(){
	return {
		templateUrl: "angularjs/directives/modal-appointment.html",
		restrict: "AE",
		scope: {
			appointment: '=data' 
		},
		controller: "ModalAppointmentController"
	};
});

angular.module('odontoSystem').controller("ModalAppointmentController", function ($scope, $http, OdontoService) {
	
	// Função responsável por abertura da modal para edição ou cadastro
	$scope.$parent.openModalAppointment = function(appointment, type){ 
		if(type === 'edit'){
			$scope.editMode = true;
			$scope.appointment = angular.copy(appointment);
			$scope.title = "Edição de Consultas"
			
			/* set dos valores que são necessários na edição */
			$scope.appointment.data = new Date($scope.appointment.data);
			$scope.appointment.horario = new Date($scope.appointment.horario);
			$scope.appointment.pacienteId = $scope.appointment.pacienteId["id"];
			$scope.appointment.status = $scope.appointment.status["id"];
			/* -------------------------------------------------------- */
		} else {
			$scope.editMode = false;
			$scope.title = "Cadastro de Consultas"
		}
		
		$("#modal-cadastro-consulta").modal("show");
	}
	
	// Removendo o objeto para limpar os campos da modal
	 $scope.$on("ClearFields", function(){
		 delete $scope.appointment;
	 });
	 
	 // Função que limpa os campos da modal
	 $scope.clearFields = function(){
	  $scope.$emit("ClearFields");
	 }
	
	// Cadastro de consultas 
	$scope.$parent.saveAppointment = function(status){
		delete $scope.appointment.editMode;
		if(status === 5){
			$("#modal-cadastro-consulta").modal("hide");
			$("#modal-cadastro-tratamento").modal("show");
		} else {
			OdontoService.save($scope.appointment, 'rest/appointment/saveAppointment', status);
		}
	}
	
	$scope.openModal = function(path, title){
		OdontoService.openModal(path);
	}

	// Função que vai carregar os pacientes cadastrados no campo select da modal de agendamento de consultas 
	$scope.loadPatientsById = function(){
		$http.get('rest/appointment/loadPatientsById')
		.then(function (response) {
			$scope.patients = response.data;
		},
	    function errorCallback(response) {
		     console.log(response);
		})
	};
	
	// Carrega os status das consultas
	$scope.loadAllStatus = function(){
		OdontoService.load('rest/appointment/loadAllStatus').then(function(response) {
			$scope.allStatus = response.data;
		},
		
	    function errorCallback(response) {
		     console.log(response);
		})
	}
	
	$scope.loadPatientsById();
	$scope.loadAllStatus();
})