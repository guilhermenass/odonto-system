app.directive("modalTreatment", function(){
	return {
		templateUrl: "angularjs/directives/modal-treatment.html",
		restrict: "AE",
		scope: {
			treatment: '=data' 
		},
		controller: "ModalTreatmentController"
	};
});

angular.module('odontoSystem').controller("ModalTreatmentController", function ($scope, $http, OdontoService) {
	
	$scope.services = [];
	
	// Removendo o objeto para limpar os campos da modal
	 $scope.$on("ClearFields", function(){
		 delete $scope.treatment;
	 });
	
	 // Função que limpa os campos da modal
	 $scope.clearFields = function(){
	  $scope.$emit("ClearFields");
	 }
	 
	/* Load de servicos na modal de tratamentos*/
	$scope.loadServices = function(){
		OdontoService.load('rest/service/loadServices').then(function(response) { 
			$scope.services = response.data;
		},function (error){
			console.log(error);
		});
	}
	 
	// Cadastro de tratamentos 
	$scope.saveTreatment = function(){
		OdontoService.save($scope.tratamento, 'rest/treatment/saveTreatment');
	}
	
	$scope.loadServices();
})