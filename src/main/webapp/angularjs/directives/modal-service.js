app.directive("modalService", function(){
	return {
		templateUrl: "angularjs/directives/modal-service.html",
		restrict: "AE",
		scope: {
			service: '=data' 
		},
		controller: "ModalServiceController"
	};
});

angular.module('odontoSystem').controller("ModalServiceController", function ($scope, $http, OdontoService) {
	
	// Função responsável por abertura da modal para edição ou cadastro
	$scope.$parent.openModalService = function(service, type){
		if(type === 'edit'){
			$scope.title = 'Edição de Serviços';
			$scope.service = angular.copy(service);
		} else {
			$scope.title = 'Cadastro de Serviços';
		}
		$("#modal-cadastro-servico").modal("show");
	}
	
	
	// Removendo o objeto para limpar os campos da modal
	 $scope.$on("ClearFields", function(){
		 delete $scope.service;
	 });
	
	 // Função que limpa os campos da modal
	 $scope.clearFields = function(){
	  $scope.$emit("ClearFields");
	 }
	
	// Cadastro de tratamentos 
	$scope.saveService = function(){
		OdontoService.save($scope.service, 'rest/service/saveService');
	}
	
	
})