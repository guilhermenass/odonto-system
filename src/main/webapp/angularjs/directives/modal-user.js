app.directive("modalUser", function(){
	return {
		templateUrl: "angularjs/directives/modal-user.html",
		restrict: "AE",
		scope: {
			user: '=data' 
		},
		controller: "ModalUserController"
	};
});


angular.module('odontoSystem').controller("ModalUserController", function ($scope, $http, OdontoService) {
	
	// Função responsável por abertura da modal para edição ou cadastro
	$scope.$parent.openModalUser = function(user, type){ 
		if(type === 'edit'){
			$scope.editMode = true;
			$scope.title = 'Edição de Usuários';
			$scope.user = angular.copy(user);
		} else {
			$scope.editMode = false;
			$scope.title = 'Cadastro de Usuários';
		}
		$("#modal-cadastro-usuario").modal("show");
	}
	
	// Cadastro de usuários 
	$scope.saveUser = function(){
		OdontoService.save($scope.user, 'rest/user/saveUser');
		$("#modal-cadastro-usuario").modal('hide');
	}
	
	// Removendo o objeto para limpar os campos da modal
	 $scope.$on("ClearFields", function(){
		 delete $scope.user;
	 });
	
	 // Função que limpa os campos da modal
	 $scope.clearFields = function(){
	  $scope.$emit("ClearFields");
	 }
})