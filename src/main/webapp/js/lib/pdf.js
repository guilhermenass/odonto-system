	var cache_width = $('#pdf').width(); //Criado um cache do CSS
    var a4  =[ 595.28,  841.89]; // Widht e Height de uma folha a4

    function getPDF(){
    // Setar o width da div no formato a4
    $("#pdf").width((a4[0]*1.33333) -80).css('max-width','none');

    // Aqui ele cria a imagem e cria o pdf
    html2canvas($('#pdf'), {
      onrendered: function(canvas) {
        var img = canvas.toDataURL("image/png",1.0);  
        var doc = new jsPDF({unit:'px', format:'a4'});
        doc.addImage(img, 'JPEG', 20, 20);
        doc.save('NOME-DO-PDF.pdf');
        //Retorna ao CSS normal
        $('#renderPDF').width(cache_width);
      }
    });
}

