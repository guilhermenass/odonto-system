$(document).ready(function(){
	var parameters = getParameters();
	if (parameters.login == "invalid") {
		bootbox.alert("Usuário e/ou senha inválidos");
	}
})

function getParameters() {
	var parameters = {};
	if (window.location.search != "") {
		var arr = window.location.search.substring(1).split("&");
		arr.forEach(function(item) {
			var arr2 = item.split("=");
			parameters[arr2[0]] = arr2[1];
		});
	}
	return parameters;
}